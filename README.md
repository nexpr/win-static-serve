# Windows 標準機能で Web サーバをたてる

- PowerShell を使用
- bin/serve.bat を実行する
- カレントディレクトリがドキュメントルート
- ポートは 8888
- 管理者として実行が必要
- Windows 7 デフォルトのバージョンは非対応
