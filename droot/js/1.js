const div = document.createElement("div")

const update = () => {
	div.textContent = new Date().toLocaleString()
}
update()

document.body.append(div)

setInterval(update, 1000)
